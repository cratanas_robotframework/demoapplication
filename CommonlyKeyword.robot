*** Settings ***
Library           Selenium2Library

*** Variables ***
${username}       ${EMPTY}
${password}       ${EMPTY}
${result}         ${EMPTY}
${login_expect}    ${EMPTY}
${isfound}         ${EMPTY}

*** Keywords ***
login to demo application
    [Arguments]    ${username}    ${password}    ${login_expect}
    Open Browser    https://ngendigital.com/demo-application    chrome
    Maximize Browser Window
    Select Frame    iframe-015
    Input Text    xpath://input[@name='email']    ${username}
    input password    xpath://input[@name='passwd']    ${password}
    Click Element    xpath://div[@name='SignIn']
    LOG    ${login_expect}
    run Keyword if      '${login_expect}'=='FOUND'      actions_check_found
    ...                     ELSE       actions_check_notfound
    [Teardown]    close browser

actions_check_found
    Select Frame        iframe-115
    Wait Until Page Contains        Book your Flight

actions_check_notfound
    Wait Until Page Contains        Invalid username/password
