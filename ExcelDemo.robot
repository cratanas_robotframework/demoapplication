*** Settings ***
Library           ExcelRobot
Library           String

*** Variables ***
${username}       ${EMPTY}
@{row_list}
${var1}           ${EMPTY}
${row_count}      ${EMPTY}

*** Test Cases ***
OpenExcel
    Open Excel    D:/robot/test1.xlsx
    ${row_count}    Get Row Count    Sheet1
    :FOR    ${var1}    IN RANGE    0    ${row_count}
    \    ${username}    Read Cell Data    Sheet1    0    ${var1}
    \    ${password}    Read Cell Data    Sheet1    1    ${var1}
    \    LOG    ${username}
