*** Settings ***
Library           Selenium2Library
Resource          CommonlyKeyword.robot
Library           ExcelRobot

*** Test Cases ***
Login1
    Open Excel    test1.xlsx
    ${row_count}    Get Row Count    Sheet1
    : FOR    ${var1}    IN RANGE    0    ${row_count}
    \    ${username}    Read Cell Data    Sheet1    0    ${var1}
    \    ${password}    Read Cell Data    Sheet1    1    ${var1}
    \    ${login_expect}    Read Cell Data    Sheet1    2    ${var1}
    \    login to demo application    ${username}    ${password}    ${login_expect}
    sleep    3s
